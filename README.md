# spelunky-chad-shops
Chad Shops is a mod to have consistent 1-2 shop contents for speedrunning.
You can select one of predefined item sets in mod settings.
The shop sign is replaced to indicate that the mod is being used.

![unknown](https://gitlab.com/Flexlolo/spelunky-chadshops/-/raw/main/images/example.png)

## Download
![unknown](https://gitlab.com/Flexlolo/spelunky-chadshops/-/raw/main/images/download.png)
